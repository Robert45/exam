import java.util.Scanner;

public class Calculator {

public static void main(String[] args) {

    Scanner input = new Scanner(System.in);

    RobMaths ref = new RobMaths();

    double answer = 0;
    double inputA, inputB;
    char operator;
    boolean done = false;

     while (done == false) {
        System.out.print("Please enter your sum: ");

        inputA = input.nextDouble();
        operator = input.next().charAt(0);
        inputB = input.nextDouble();        

        switch (operator) {
            case '+': answer =  ref.add(inputA, inputB);
                      break;
            case '-': answer =  ref.subtract(inputA, inputB);
                      break;
            case '*': answer =  ref.multiply(inputA, inputB);
                      break;
            case '/': answer =  ref.divide(inputA, inputB);
                      break;
            case '^': answer =  ref.power(inputA, inputB);
                      break;
        }

            System.out.println(answer);             
    }       

    input.close();

  }

}